﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MediaOrganiser.Favourites
{
    interface IFavourites
    {
        string Name { get; set; }
        string Path { get; set; }
    }
}
