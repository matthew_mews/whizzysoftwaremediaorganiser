﻿using MediaOrganiser.Favourites;
using System;
using System.Collections.Generic;
using System.Text;


public class FavouritesModel : IFavourites
{
    public string Name { get; set; }
    public string Path { get; set; }

    public FavouritesModel(string name, string path)
    {
        this.Name = name;
        this.Path = path;
    }
}
