﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

public class Settings
{
    private static string WhizzySoftwareDirectoryPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "/Whizzy Software";
    private static string WhizzySoftwareSettingsPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "/Whizzy Software/settings.json";

    public SettingsModel GetSettings()
    {
        if (!WhizzySoftwareDirectoryExists())
        {
            Directory.CreateDirectory(WhizzySoftwareDirectoryPath);
        }

        return !WhizzySoftwareSettingsFileExists() ? CreateSettings() : LoadSettings();
    }

    private static bool WhizzySoftwareDirectoryExists()
    {
        return Directory.Exists(WhizzySoftwareDirectoryPath) ? true : false;
    }

    private static bool WhizzySoftwareSettingsFileExists()
    {
        return File.Exists(WhizzySoftwareSettingsPath) ? true : false;
    }

    private SettingsModel CreateSettings()
    {
        SettingsModel settings = new SettingsModel(Environment.MachineName, Environment.UserName, new List<MediaInfoModel>(), new List<FavouritesModel>());

        File.WriteAllText(WhizzySoftwareSettingsPath, JsonConvert.SerializeObject(settings, Formatting.Indented));

        return settings;
    }

    private SettingsModel LoadSettings()
    {
        string serializedSettings = File.ReadAllText(WhizzySoftwareSettingsPath);

        SettingsModel settings = JsonConvert.DeserializeObject<SettingsModel>(serializedSettings);

        return settings;
    }

    private void UpdateSettings(SettingsModel settings)
    {
        File.WriteAllText(WhizzySoftwareSettingsPath, JsonConvert.SerializeObject(settings, Formatting.Indented));
    }

    public void AddNewMediaInformation(MediaInfoModel mediaInfo)
    {
        List<MediaInfoModel> newMediaInformation = new List<MediaInfoModel>();
        SettingsModel currentSettings = LoadSettings();
        List<MediaInfoModel> currentMachineInformation = currentSettings.MediaInformation;

        List<MediaInfoModel> result = currentMachineInformation.Where(c => c.FilePath == mediaInfo.FilePath).ToList();

        if (result.Count > 0)
        {
            List<MediaInfoModel> tempList = new List<MediaInfoModel>();

            foreach (var cmi in currentMachineInformation)
            {
                if (cmi.FilePath == mediaInfo.FilePath)
                {
                    cmi.Comment = mediaInfo.Comment;
                    cmi.MusicGenres = mediaInfo.MusicGenres;
                }

                if (cmi.FilePath != String.Empty && cmi.MusicGenres != String.Empty)
                {                
                    tempList.Add(new MediaInfoModel(cmi.FilePath, cmi.Comment, cmi.MusicGenres));
                }
            }
         
            newMediaInformation.AddRange(tempList);
        }
        else 
        { 
            newMediaInformation.AddRange(currentSettings.MediaInformation);
            newMediaInformation.Add(mediaInfo);
        }

        UpdateSettings(new SettingsModel(Environment.MachineName, Environment.UserName, newMediaInformation, currentSettings.Favourites));
    }

    public void AddFavourite(FavouritesModel favourite)
    {
        List<FavouritesModel> newFavourites = new List<FavouritesModel>();
        SettingsModel currentSettings = LoadSettings();
        List<FavouritesModel> currentFavourites = currentSettings.Favourites;

        List<FavouritesModel> result = currentFavourites.Where(c => c.Path== favourite.Path).ToList();

        if (result.Count == 0)
        {
            newFavourites.AddRange(currentFavourites);
            newFavourites.Add(favourite);
            UpdateSettings(new SettingsModel(Environment.MachineName, Environment.UserName, currentSettings.MediaInformation, newFavourites));
        }
    }
    
    public void DeleteFavourite(FavouritesModel favourite)
    {
        List<FavouritesModel> newFavourites = new List<FavouritesModel>();
        SettingsModel currentSettings = LoadSettings();
        List<FavouritesModel> currentFavourites = currentSettings.Favourites;

        foreach (FavouritesModel cfav in currentFavourites)
        {
            if (cfav.Name != favourite.Name && cfav.Path != favourite.Path)
            {
                newFavourites.Add(new FavouritesModel(cfav.Name, cfav.Path));
            }
        }

        UpdateSettings(new SettingsModel(Environment.MachineName, Environment.UserName, currentSettings.MediaInformation, newFavourites));
    }
}