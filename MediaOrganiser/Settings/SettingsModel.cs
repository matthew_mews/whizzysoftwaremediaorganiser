﻿
using System.Collections.Generic;
using System.Windows.Documents;

public class SettingsModel : ISettings
{
    public string MachineName { get; }
    public string UserName { get; }
    public List<MediaInfoModel> MediaInformation { get; set; }
    public List<FavouritesModel> Favourites { get; set; }

    public SettingsModel(string machineName, string userName, List<MediaInfoModel> mediaInformation, List<FavouritesModel> favourites)
    {
        this.MachineName = machineName;
        this.UserName = userName;
        this.MediaInformation = mediaInformation;
        this.Favourites = favourites;
    }
}

