﻿
using System.Collections.Generic;

interface ISettings
{
    string MachineName { get; }
    string UserName { get; }
    List<MediaInfoModel> MediaInformation { get; set; }
    List<FavouritesModel> Favourites { get; set; }
}

