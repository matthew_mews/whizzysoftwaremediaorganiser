﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Sorting;

namespace MediaOrganiser
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string currentPath { get; set; } = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic);
        private string previousPath { get; set; } = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic);
        private string forwardPath { get; set; } = String.Empty;

        public static Settings settings = new Settings();
        private List<MediaModel> mediaItems = new List<MediaModel>();
        private string mediaFileSource = String.Empty;

        public MainWindow()
        {
            settings.GetSettings();

            InitializeComponent();

            listViewResults.ItemsSource = mediaItems;

            PerformScan();
        }

        private void PerformScan()
        {
            // Set the search bar's text to the current path.
            txtSearchBar.Text = currentPath;

            ClearPreviousResults();
            GetFavourites();

            try
            {
                GetDirectories();
                GetFiles();
            } catch (UnauthorizedAccessException)
            {
                // Ignore unauthorized directories
                return;
            }

            listViewResults.Items.Refresh();
        }

        private void GetFavourites()
        {
            List<FavouritesModel> favourites = settings.GetSettings().Favourites;

            stkFavourites.Children.Clear();

            for (int i = 0; i < favourites.Count; i++)
            {
                Button newBtn = new Button();
                Button delBtn = new Button();

                newBtn.Content = favourites[i].Name;
                newBtn.Tag = favourites[i].Path;
                newBtn.Margin = new Thickness(0, 0, 0, 5);
                newBtn.Click += new RoutedEventHandler(btnFavourite_Click);

                delBtn.Content = "Delete";
                delBtn.Tag = favourites[i].Path;
                delBtn.Margin = new Thickness(0, 0, 0, 20);
                delBtn.Click += new RoutedEventHandler(btnDeleteFavourite_Click);
                delBtn.Foreground = System.Windows.Media.Brushes.Red;
                delBtn.FontWeight = FontWeights.Bold;

                stkFavourites.Children.Add(newBtn);
                stkFavourites.Children.Add(delBtn);
            }
        }

        private void btnFavourite_Click(object sender, RoutedEventArgs e)
        {
            string filePath = (sender as Button).Tag.ToString();

            if (Directory.Exists(filePath))
            {
                previousPath = currentPath;
                currentPath = filePath;
                PerformScan();
            } else if (File.Exists(filePath))
            {
                PlayMedia(Path.GetExtension(filePath), filePath);
            }
        }
        private void btnDeleteFavourite_Click(object sender, RoutedEventArgs e)
        {
            Button button = (sender as Button);

            settings.DeleteFavourite(new FavouritesModel(button.Content.ToString(), button.Tag.ToString()));
            GetFavourites();
        }

        private void GetDirectories()
        {
            foreach (string item in Directory.GetDirectories(currentPath))
            {
                DirectoryInfo di = new DirectoryInfo(item);
                mediaItems.Add(new MediaModel(di.Name, di.FullName, di.LastWriteTime, String.Empty, null));
            }
        }

        private void GetFiles()
        {
            foreach (string item in Directory.GetFiles(currentPath)) {

                FileInfo fi = new FileInfo(item);

                if (IsValidMedia(fi.Extension)) {

                    bool mediaFilterMp3 = (bool)chkMediaFilterMP3.IsChecked;
                    bool mediaFilterFlac = (bool)chkMediaFilterFLAC.IsChecked;
                    bool mediaFilterAvi = (bool)chkMediaFilterAVI.IsChecked;
                    bool mediaFilterMp4 = (bool)chkMediaFilterMP4.IsChecked;
                    bool mediaFilterMkv = (bool)chkMediaFilterMKV.IsChecked;
                    bool mediaFilterAac = (bool)chkMediaFilterAAC.IsChecked;
                    bool mediaFilterWav = (bool)chkMediaFilterWAV.IsChecked;
                    bool mediaFilterJpg = (bool)chkMediaFilterJPG.IsChecked;
                    bool mediaFilterJpeg = (bool)chkMediaFilterJPEG.IsChecked;
                    bool mediaFilterTif = (bool)chkMediaFilterTIF.IsChecked;
                    bool mediaFilterGif = (bool)chkMediaFilterGIF.IsChecked;
                    bool mediaFilterPng = (bool)chkMediaFilterPNG.IsChecked;

          
                    List<string> mediaFilterExtensions = MediaFilters.GetMediaFilters(mediaFilterMp3, mediaFilterFlac, mediaFilterAvi, mediaFilterMp4, mediaFilterMkv,
                        mediaFilterAac, mediaFilterWav, mediaFilterJpg, mediaFilterJpeg, mediaFilterPng, mediaFilterTif, mediaFilterGif);

                    if (mediaFilterExtensions.Contains(fi.Extension.ToLower()))
                    {
                        mediaItems.Add(new MediaModel(fi.Name, fi.FullName, fi.LastWriteTime, fi.Length.ToString(), fi.Extension));
                    }
                }
            }
        }
        private void ClearPreviousResults()
        {
            // Clear media items array
            mediaItems.Clear();
        }

        // Dock panel menu
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem btnSender = (MenuItem)sender;

            switch (btnSender.Header.ToString())
            {
                case "_Open Directory": ShowFolderBrowserDialog(); break;
                case "_Exit": Environment.Exit(0); break;
                case "_About Us": MessageBox.Show("Copyright 2020 Whizzy Software."); break;
            }
        }

        private void ShowFolderBrowserDialog()
        {
            using (var fbd = new System.Windows.Forms.FolderBrowserDialog() { Description = "Please select a directory" })
            {
                if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    previousPath = currentPath;
                    currentPath = fbd.SelectedPath;
                    PerformScan();
                }
            }
        }

        private void btnForward_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(forwardPath))
            {
                currentPath = forwardPath;
                PerformScan();
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            forwardPath = currentPath;
            currentPath = previousPath;
            PerformScan();
        }

        private void btnTopLevel_Click(object sender, RoutedEventArgs e)
        {
            if (currentPath != @"C:\")
            {
                currentPath = Directory.GetParent(currentPath).FullName;
                PerformScan();
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            HandleSearchBarSearch();
        }

        private void MediaPlayerPlay_Click(object sender, RoutedEventArgs e)
        {
            if (listViewResults.SelectedIndex <= 0) return;

            MediaModel item = mediaItems[listViewResults.SelectedIndex];

            PlayMedia(item.Extension, item.Path);
        }

        private void PlayMedia(string extension, string path)
        {
            if (IsValidMedia(extension))
            {
                MediaControlVisibility(true);
                UpdateMediaPlayerSource(path);

                if (IsMusicFile(extension))
                {
                    string albumCoverSrc = GetAlbumCover(Directory.GetParent(path).FullName);

                    if (albumCoverSrc != null)
                    {
                        var albumCoverUri = new Uri(albumCoverSrc, UriKind.Relative);
                        imgAlbumCover.ImageSource = new BitmapImage(albumCoverUri);
                        mediaElement.Height = 350;
                    }

                    mediaElement.Play();

                }
                else if (IsImageFile(extension))
                {
                    mediaElement.Height = 350;
                    var image = new Uri(path, UriKind.Relative);
                    imgAlbumCover.ImageSource = new BitmapImage(image);
                    MediaControlVisibility(false);

                } else
                {
                    mediaElement.Height = 350;
                    imgAlbumCover.ImageSource = null;
                    mediaElement.Play();
                }

                expMediaPlayer.IsExpanded = true;
                
            }
        }

        private void MediaControlVisibility(bool enabled)
        {
            btnMediaPlayerPlay.IsEnabled = enabled ? true : false;
            btnMediaPlayerPause.IsEnabled = enabled ? true : false;
            btnMediaPlayerStop.IsEnabled = enabled ? true : false;
            sliderVolume.IsEnabled = enabled ? true : false;
            sliderSeek.IsEnabled = enabled ? true : false;
        }

        private string GetAlbumCover(string filePath)
        {
            string[] imageExtensons = { ".jpg", ".jpeg", ".png", ".gif" };

            var imageSearchResults = Directory.GetFiles(filePath).Where(file => imageExtensons.Any(file.ToLower().EndsWith)).ToList();

            return imageSearchResults.Count > 0 ? imageSearchResults[0] : null;
        }
        
        private void UpdateMediaPlayerSource(string filePath)
        {
            // If the filePath is the same the media source, don't reset the source.
            // This prevents the media from restarting from the beginning.
            if (filePath != mediaFileSource) {
                mediaElement.Source = new Uri(filePath);
                mediaFileSource = filePath;
            }
        }

        private bool IsValidMedia(string extension)
        {
            switch (extension)
            {
                case ".mp3": return true;
                case ".flac": return true;
                case ".avi": return true;
                case ".mp4": return true;
                case ".mkv": return true;
                case ".aac": return true;
                case ".wav": return true;
                case ".jpg": return true;
                case ".jpeg": return true;
                case ".png": return true;
                case ".tif": return true;
                case ".gif": return true;

                default: return false;
            }
        }

        private bool IsMusicFile(string extension)
        {
            switch (extension)
            {
                case ".mp3": return true;
                case ".flac": return true;
                case ".aac":  return true;
                case ".wav": return true;

                default: return false;
            }
        }

        private bool IsImageFile(string extension)
        {
            switch (extension)
            {
                case ".jpg": return true;
                case ".jpeg": return true;
                case ".png": return true;
                case ".tif": return true;
                case ".gif": return true;

                default: return false;
            }
        }

        private void MediaPlayerPause_Click(object sender, RoutedEventArgs e)
        {
            mediaElement.Pause();
        }

        private void MediaPlayerStop_Click(object sender, RoutedEventArgs e)
        {
            mediaElement.Stop();
            mediaElement.Height = 0;
            expMediaPlayer.IsExpanded = false;
        }

        private void sliderVolume_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            mediaElement.Volume = (double)sliderVolume.Value;
        }

        private void sliderSeek_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int sliderValue = (int)this.sliderSeek.Value;
            TimeSpan ts = new TimeSpan(0, 0, 0, sliderValue, 0);
            this.mediaElement.Position = ts;
        }

        private void mediaElement_MediaOpened(object sender, RoutedEventArgs e)
        {
            if (sliderSeek.IsEnabled)
            {
                this.sliderSeek.Maximum = this.mediaElement.NaturalDuration.TimeSpan.TotalSeconds;
            }
        }

        private void HandleItemClick()
        {
            if (listViewResults.SelectedIndex < 0) return;

            MediaModel item = mediaItems[listViewResults.SelectedIndex];

            if (File.Exists(item.Path))
            {
                try
                {
                    PlayMedia(item.Extension, item.Path);
                } catch (Exception UnableToOpenProgramException)
                {
                    MessageBox.Show($"Unable to play media {item.Name}.\n" + UnableToOpenProgramException, "Media Play Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            } else if (Directory.Exists(item.Path))
            {
                previousPath = currentPath;
                currentPath = item.Path;
                PerformScan();
            }
        }

        private void listResultsView_KeyDown(object sender, KeyEventArgs e)
        {
            switch(e.Key)
            {
                case Key.Return: HandleItemClick(); break;
                case Key.Delete: HandleItemDeletion(); break;

                default: break;
            }
        }

        private void txtSearchBar_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Return: HandleSearchBarSearch(); break;
           
                default: break;
            }
        }

        private void HandleItemDeletion()
        {
            if (listViewResults.SelectedIndex < 0) return;

            MediaModel item = mediaItems[listViewResults.SelectedIndex];

            try
            {
                if (MessageBox.Show($"Are you sure you wish to delete {item.Name}?", "Item Deletion", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes) {
                    if (File.Exists(item.Path))
                    {
                        File.Delete(item.Path);
                    } else if (Directory.Exists(item.Path))
                    {
                        Directory.Delete(item.Path);
                    }
                }
            } catch (Exception UnableToDeleteItemException)
            {
                MessageBox.Show($"Unable to open delete {item.Name}.\n" + UnableToDeleteItemException, "Deletion Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                PerformScan();
            }
        }

        private void HandleSearchBarSearch()
        {
            if (Directory.Exists(txtSearchBar.Text))
            {
                previousPath = currentPath;
                currentPath = txtSearchBar.Text;
                PerformScan();
            }
        }
        private void HandleItemSort(object sender, RoutedEventArgs e)
        {
            var item = (GridViewColumnHeader) sender;
            string itemName = item.Tag.ToString();

            SortItems.HandleSort(itemName, mediaItems);
            listViewResults.Items.Refresh();
        }

        private void ListViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListViewItem item = sender as ListViewItem;
            if (item != null && item.IsSelected)
            {
                HandleItemClick();
            }
        }

        private void btnSaveMediaInformationChanges_Click(object sender, RoutedEventArgs e)
        {
            if (listViewResults.SelectedIndex < 0) return;

            MediaModel item = mediaItems[listViewResults.SelectedIndex];

            settings.AddNewMediaInformation(new MediaInfoModel(item.Path, txtComment.Text, txtGenres.Text));
        }

        private void listResultsView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listViewResults.SelectedIndex < 0) return;

            MediaModel item = mediaItems[listViewResults.SelectedIndex];

            // Check for media information.
            List<MediaInfoModel> mediaInformation = settings.GetSettings().MediaInformation;
            List<MediaInfoModel> result = mediaInformation.Where(m => m.FilePath == item.Path).ToList();

            if (result.Count > 0)
            {
                txtComment.Text = result[0].Comment;
                txtGenres.Text = result[0].MusicGenres;
            }
            else
            {
                txtComment.Text = String.Empty;
                txtGenres.Text = String.Empty;
            }
        }

        private void btnAddFavourite_Click(object sender, RoutedEventArgs e)
        {
            if (listViewResults.SelectedIndex < 0) return;

            MediaModel item = mediaItems[listViewResults.SelectedIndex];

            settings.AddFavourite(new FavouritesModel(item.Name, item.Path));

            GetFavourites();
        }

        private void chkMediaFilter_Click(object sender, RoutedEventArgs e)
        {
            PerformScan();
        }

        private void HandleQuickLink_Click(object sender, RoutedEventArgs e)
        {
            string btnName = (sender as Button).Tag.ToString();
            previousPath = currentPath;

            switch (btnName)
            {
                case "Music": currentPath = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic); break;
                case "Videos": currentPath = Environment.GetFolderPath(Environment.SpecialFolder.MyVideos); break;
                case "Pictures": currentPath = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures); break;
            }

            PerformScan();
        }
    }
}