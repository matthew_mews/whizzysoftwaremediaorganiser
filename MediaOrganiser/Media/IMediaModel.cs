﻿using System;
using System.Collections.Generic;
using System.Text;

interface IMediaModel
{
    string Name { get; }
    string Path { get; }
    string Size { get; }
    string Extension { get; }
}