﻿using System;

public class MediaModel : IMediaModel
{
    public string Name { get; }
    public string Path { get; }
    public DateTime DateModified { get; }
    public string Size { get; }
    public string Extension { get; }

    public MediaModel(string name, string path, DateTime dateModified, string size, string extension)
    {
        this.Name = name;
        this.Path = path;
        this.DateModified = dateModified;
        this.Size = size;
        this.Extension = extension;
    }
}
