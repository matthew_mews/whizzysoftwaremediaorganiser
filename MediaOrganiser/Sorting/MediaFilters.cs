﻿using System;
using System.Collections.Generic;

namespace Sorting
{
    public class MediaFilters
    {
        public static List<string> GetMediaFilters(bool mp3IsChecked, bool flacIsChecked, bool aviIsChecked, bool mp4IsChecked, bool mkvIsChecked, bool aacIsChecked
            , bool wavIsChecked, bool jpgIsChecked, bool jpegIsChecked, bool pngIsChecked, bool tifIsChecked, bool gifIsChecked)
        {
            List<string> allowedExtensions = new List<string>();

            if (mp3IsChecked == true)
            {
                allowedExtensions.Add(".mp3");
            }

            if (flacIsChecked == true)
            {
                allowedExtensions.Add(".flac");
            }

            if (aviIsChecked == true)
            {
                allowedExtensions.Add(".avi");
            }

            if (mp4IsChecked == true)
            {
                allowedExtensions.Add(".mp4");
            }

            if (mkvIsChecked == true)
            {
                allowedExtensions.Add(".mkv");
            }

            if (aacIsChecked == true)
            {
                allowedExtensions.Add(".aac");
            }

            if (wavIsChecked == true)
            {
                allowedExtensions.Add(".wav");
            }

            if (jpgIsChecked == true)
            {
                allowedExtensions.Add(".jpg");
            }

            if (jpegIsChecked == true)
            {
                allowedExtensions.Add(".jpeg");
            }

            if (pngIsChecked == true)
            {
                allowedExtensions.Add(".png");
            }

            if (tifIsChecked == true)
            {
                allowedExtensions.Add(".tif");
            }

            if (gifIsChecked == true)
            {
                allowedExtensions.Add(".gif");
            }

            return allowedExtensions;
        }
    }
}
