﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Sorting { 
public class SortItems
{
    private static List<MediaModel> sortedItems = new List<MediaModel>();
    private static bool nameAsc = false;
    private static bool dateModifiedAsc = true;
    private static bool sizeAsc = true;
    private static bool extensionAsc = true;
    public static void HandleSort(string itemName, List<MediaModel> mediaItems)
    {
        switch(itemName)
        {
            case "Name":
                sortedItems.AddRange(SortByName(mediaItems));
                nameAsc = !nameAsc;
                break;

            case "DateModified": 
                sortedItems.AddRange(SortByDateModified(mediaItems));
                dateModifiedAsc = !dateModifiedAsc;
                break;

            case "Size":
                sortedItems.AddRange(SortBySize(mediaItems));
                sizeAsc = !sizeAsc;
                break;

            case "Extension":
                sortedItems.AddRange(SortByExtension(mediaItems));
                extensionAsc = !extensionAsc;
                break;

            default:
                sortedItems = mediaItems;
                break;

        }

        mediaItems.Clear();
        mediaItems.AddRange(sortedItems);
        sortedItems.Clear();
    }

    private static List<MediaModel> SortByName(List<MediaModel> mediaItems)
    {
        return nameAsc ? mediaItems.OrderBy(i => i.Name).ToList() : mediaItems.OrderBy(i => i.Name).Reverse().ToList();
    }

    private static List<MediaModel> SortByDateModified(List<MediaModel> mediaItems)
    {
        return dateModifiedAsc ? mediaItems.OrderBy(i => i.DateModified).ToList() : mediaItems.OrderBy(i => i.DateModified).Reverse().ToList();
    }
    private static List<MediaModel> SortBySize(List<MediaModel> mediaItems)
    {
        return sizeAsc ? mediaItems.OrderBy(i => i.Size).ToList() : mediaItems.OrderBy(i => i.Size).Reverse().ToList();
    }
    private static List<MediaModel> SortByExtension(List<MediaModel> mediaItems)
    {
        return extensionAsc ? mediaItems.OrderBy(i => i.Extension).ToList() : mediaItems.OrderBy(i => i.Extension).Reverse().ToList();
    }
}
}