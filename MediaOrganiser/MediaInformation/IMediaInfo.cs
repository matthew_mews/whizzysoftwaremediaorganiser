﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MediaOrganiser.MediaInformation
{
    interface IMediaInfo
    {
        string Comment { get; set; }
        string MusicGenres { get; set; }
    }
}
