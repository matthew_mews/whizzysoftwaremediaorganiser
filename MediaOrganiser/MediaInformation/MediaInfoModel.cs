﻿using MediaOrganiser.MediaInformation;
using System;
using System.Collections.Generic;
using System.Text;

public class MediaInfoModel : IMediaInfo
{
    public string FilePath { get; set; }
    public string Comment { get; set; }
    public string MusicGenres { get; set; }

    public MediaInfoModel(string filePath, string comment, string musicGenres)
    {
        this.FilePath = filePath;
        this.Comment = comment;
        this.MusicGenres = musicGenres;
    }
}

